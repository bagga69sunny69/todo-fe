import React, { useState, ChangeEvent, FormEvent } from 'react'
import './../styles/Login.css'
import { useNavigate } from 'react-router-dom'
import { LoginFormData } from '../interfaces/LoginFormData'
import { isValidEmail } from '../utils/helpers/isValidEmail'
import { toast } from 'react-hot-toast'
import api from '../utils/api'

type LoginProps = {}

const Login: React.FC<LoginProps> = (): React.ReactElement => {
  const navigate = useNavigate()
  const navigateToRegisterPage = () => navigate('/signup')
  const [formData, setFormData] = useState<LoginFormData>({ email: '', password: '' })
  const [loading, setLoading] = useState<boolean>(false)
  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const { email, password } = formData

    if (!email || !isValidEmail(email)) {
      toast.error('Use a valid email')
      return
    } else if (!password || password.trim().length < 8) {
      toast.error('Invalid Password')
      return
    } else {
      setLoading(true)
      try {
        const res = await api.post('/login', formData)
        toast.success(res.data.message)
      } catch (error) {
        toast.error('Something went wrong!')
      } finally {
        setLoading(false)
      }
    }
  }

  return (
    <div className="login">
      <header className="login__header">
        <div className="login__header__content">
          <h1 className="login__header__content__heading">NoteStix.</h1>
          <button className="login__header__content__btn" onClick={navigateToRegisterPage}>
            Sign Up
          </button>
        </div>
      </header>
      <section className="login__form">
        <div className="login__form__content">
          <h2 className="login__form__content__header">Welcome Back!</h2>
          <p className="login__form__content__description">Please enter your email and password</p>
          <form onSubmit={handleSubmit} className="login__form__content__form">
            <div>
              <label htmlFor="email">Email</label>
              <input
                type="email"
                id="email"
                autoComplete="off"
                placeholder="notestix@email.com"
                value={formData.email}
                onChange={(e: ChangeEvent<HTMLInputElement>) =>
                  setFormData((prev) => {
                    return { ...prev, email: e.target.value }
                  })
                }
                required
              />
            </div>
            <div>
              <label htmlFor="password">Password</label>
              <input
                type="password"
                id="password"
                autoComplete="off"
                placeholder="password (atleast 8 characters)"
                value={formData.password}
                onChange={(e: ChangeEvent<HTMLInputElement>) =>
                  setFormData((prev) => {
                    return { ...prev, password: e.target.value }
                  })
                }
                required
              />
            </div>
            <button className={`login__header__content__btn w-full ${loading ? 'disable-btn' : ''}`} disabled={loading}>
              <span>Log In</span>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="icon"
              >
                <path strokeLinecap="round" strokeLinejoin="round" d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
              </svg>
            </button>
          </form>
        </div>
      </section>
      <section className="login__form text-center">
        <p>
          {new Date().getFullYear()} | &copy;{' '}
          <a href="https://github.com/vipul-vaishnav/" target="_blank" rel="noopener noreferrer" className="link">
            Vipul Vaishnav
          </a>{' '}
        </p>
      </section>
    </div>
  )
}
export default Login
