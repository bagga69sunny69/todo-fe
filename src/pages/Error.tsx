import React from 'react'
import './../styles/Error.css'
import { useNavigate } from 'react-router-dom'

const Error: React.FC = (): React.ReactElement => {
  const navigate = useNavigate()
  const moveBack = () => navigate(-1)

  return (
    <div className="error">
      <h1>NoteStix.</h1>
      <div>
        <h2>404</h2>
        <p>Oops! Page Not Found</p>
      </div>
      <button onClick={moveBack}>Go Back</button>
    </div>
  )
}
export default Error
