import React from 'react'
import { Routes, Route } from 'react-router-dom'
import Login from './pages/Login'
import Register from './pages/Register'
import Error from './pages/Error'
import { Toaster } from 'react-hot-toast'

const App: React.FC = (): React.ReactElement => {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/signup" element={<Register />} />
        <Route path="*" element={<Error />} />
      </Routes>
      <Toaster />
    </div>
  )
}
export default App
