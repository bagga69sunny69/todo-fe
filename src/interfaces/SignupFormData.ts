export interface SignupFormData {
  email: string
  password: string
  firstName: string
  lastName: string
}
